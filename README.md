## Ducky Scripts

Howdy!  Thanks for checking out Ducky Scripts; a collection of customizable scripts for use with the USB Rubber Ducky, aimed mostly at educating users of the impact of physical security.  Here are the scripts you'll find here:
- Captive Video: Locks the target computer, and waits for the user to unlock it again.  Once that happens, the script will take the target computer hostage, playing a video that's difficult to stop or exit.
- Set Language: Changes the input language to a language that is very different than English (US).  Makes for a good laugh when the target tries to type again, especially at the login screen.
- More to come!

To learn more about what the USB Rubber Ducky is, check out: [https://shop.hak5.org/products/usb-rubber-ducky-deluxe](https://shop.hak5.org/products/usb-rubber-ducky-deluxe)

*-James*