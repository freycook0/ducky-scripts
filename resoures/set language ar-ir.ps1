# This script isn't actually used anywhere, it's just an educational reference for the "set language ar-ir.txt" ducky script.
$text = @'
$LanguageList = Get-WinUserLanguageList
# We're assuming there are only 6 languages in the user's preferences.
$LanguageList.Clear()
$LanguageList.Add('AR-IR')
Set-WinUserLanguageList $LanguageList
Y
exit

'@
$text | powershell.exe
