# Objective is to download the portable (.tar.gz) version of Java SE JRE for Windows (x64), un-pack it, and place the JRE directory in the root of the Ducky Scripts.  This will enable the compilation of the Ducky Scripts using the compilation scripts.

# Need to set the TLS protocol version to 1.2, otherwise, PS will fail to connect to GitHub over HTTPS.
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12

# Root of the Ducky Scripts folder (where the project is downloaded to).
$rootFolder = ((Get-Item ((pwd).Path)).Parent).FullName

# View the available Java SE JRE downloads by Oracle here:
# https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html
# This is the URL of the download of (Oracle's) Java SE JRE 8 update 202, portable, for Windows (x64).  The URL will need to be updated if a different version/update is required or desired.
# $javaUrl = "https://download.oracle.com/otn-pub/java/jdk/8u202-b08/1961070e4c9b4e26a04e7f5a083f551e/jre-8u202-windows-x64.tar.gz"

# This is the URL of the download of (OpenJDK's) Java SE JRE 8 update 202 build 8 (HopSpot JVM).  The URL will need to be updated if a different version/update is required or desired.
$javaUrl = "https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u212-b04/OpenJDK8U-jre_x64_windows_hotspot_8u212b04.zip"
# Aim to download the file to the root of the Ducky Scripts folder.
# $javaFile = $rootFolder + "\jre-8u202-windows-x64.tar.gz"
$javaFile = "$rootFolder\OpenJDK8U-jre_x64_windows_hotspot_8u202b08.zip"
$javaClient = New-Object System.Net.WebClient
# This cookie is no longer necessary, as the script points to the OpenJDK JRE, instead of the Oracle JRE.  Leaving as a comment for documentation.
# Set the "Accept License" cookie, required to download the file (you'll get a 403 otherwise.
# $javaClient.Headers.Add([System.Net.HttpRequestHeader]::Cookie,"oraclelicense=accept-securebackup-cookie")
$javaClient.DownloadFile($javaUrl,$javaFile)

# Unpack the OpenJDK zip archive.
Expand-Archive -LiteralPath "$javaFile" -DestinationPath "$rootFolder"
$javaFolder = "$rootFolder\jdk8u212-b04-jre"
# Rename the folder to "java" (simplifies maintaining the compilation scripts).
Rename-Item -LiteralPath "$javaFolder" -NewName "$rootFolder\java" -Force
# Delete the zip archive that we no longer need.
Remove-Item -LiteralPath "$javaFile" -Force

# This is the URL of the download of the duck encoder.  I believe this URL is stable, and will download the latest version of the encoder.
$encoderURL = "https://github.com/hak5darren/USB-Rubber-Ducky/raw/master/duckencoder.jar"
$encoderFile = "$rootFolder\duckencoder.jar"
$encoderClient = New-Object System.Net.WebClient
$encoderClient.DownloadFile($encoderUrl,$encoderFile)
